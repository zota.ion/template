package eu.openstock.manager.service;

import com.coxautodev.graphql.tools.SchemaParser;
import eu.openstock.manager.graphql.Query;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import java.io.IOException;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class GraphqlService {

  private GraphQL graphql;
  @Autowired
  private UserService userService;


  @PostConstruct
  private void loadSchema() throws IOException {

    graphql = GraphQL.newGraphQL(buildSchema()).build();
  }

  private GraphQLSchema buildSchema() {
    return SchemaParser.newParser()
        .file("schema.graphqls")
        .resolvers(
            new Query(userService)

        ).build()
        .makeExecutableSchema();
  }

  public GraphQL getGraphql() {
    return graphql;
  }
}
