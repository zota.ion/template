package eu.openstock.manager.service.impl;

import eu.openstock.manager.common.RoleName;
import eu.openstock.manager.model.Role;
import eu.openstock.manager.repository.RoleRepository;
import eu.openstock.manager.service.RoleService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

  @Autowired
  private RoleRepository roleRepository;

  @Override
  public Optional<Role> findByName(RoleName roleName) {
    return roleRepository.findRoleByName(roleName.name());
  }
}
