package eu.openstock.manager.service;

import eu.openstock.manager.model.User;
import java.util.Optional;

public interface UserService {

  Optional<User> save(User user);

  Optional<User> findByUsername(String username);

  Optional<User> findByUsernameOrEmail(String username, String email);

  Optional<User> findById(Long id);

  boolean existsByUsername(String username);

  boolean existsByEmail(String username);

}
