package eu.openstock.manager.security;

import eu.openstock.manager.exception.ResourceNotFoundException;
import eu.openstock.manager.model.User;
import eu.openstock.manager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class CustomUserDetailsService implements UserDetailsService {

  @Autowired
  UserService userService;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String usernameOrEmail)
      throws UsernameNotFoundException {
    // Let people login with either username or email
    User user = userService.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
        .orElseThrow(() ->
            new UsernameNotFoundException(
                "User not found with username or email : " + usernameOrEmail)
        );

    return UserPrincipal.create(user);
  }

  /**
   * loadUserById.
   * @param id userId
   * @return UserDetails
   */
  @Transactional
  public UserDetails loadUserById(Long id) {
    User user = userService.findById(id).orElseThrow(
        () -> new ResourceNotFoundException("User", "id", id)
    );

    return UserPrincipal.create(user);
  }
}