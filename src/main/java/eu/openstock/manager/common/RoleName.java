package eu.openstock.manager.common;


public enum RoleName {
  ROLE_USER,
  ROLE_ADMIN
}
