package eu.openstock.manager.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class JwtAuthenticationResponse {

  private String accessToken;
  private String tokenType = "Bearer";


}
