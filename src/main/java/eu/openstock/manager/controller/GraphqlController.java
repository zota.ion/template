package eu.openstock.manager.controller;

import eu.openstock.manager.service.GraphqlService;
import eu.openstock.manager.utill.QueryParameters;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/graphql")
@RestController
public class GraphqlController {

  @Autowired
  GraphqlService graphqlService;

  @PostMapping
  public ResponseEntity<Object> postRequest(@RequestBody String query) {

    return new ResponseEntity<>(processRequest(query), HttpStatus.OK);
  }

  @GetMapping
  public ResponseEntity<Object> getRequest(@RequestBody String query) {

    return new ResponseEntity<>(processRequest(query), HttpStatus.OK);
  }

  private ExecutionResult processRequest(String query) {

    QueryParameters parameters = QueryParameters.from(query);

    ExecutionInput executionInput = ExecutionInput.newExecutionInput()
        .query(parameters.getQuery())
        .variables(parameters.getVariables())
        .operationName(parameters.getOperationName())
        .build();

    ExecutionResult result = graphqlService.getGraphql().execute(executionInput);
    return result;
  }
}
